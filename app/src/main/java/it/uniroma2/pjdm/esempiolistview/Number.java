package it.uniroma2.pjdm.esempiolistview;

/**
 * Created by clauz on 5/17/18.
 */

public class Number {
    private int number;

    public Number(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Numero " + number;
    }

    public String getInfo() {
        double square = number * number;
        double squareRoot = Math.sqrt(number);

        String result = "";
        result += "Informazioni sul numero " + number + ":\n\n";
        result += "quadrato: " + square + "\n";
        result += "radice quadrata: " + squareRoot + "\n";

        return result;
    }
}










