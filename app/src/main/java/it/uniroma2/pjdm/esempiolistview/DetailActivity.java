package it.uniroma2.pjdm.esempiolistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Bundle extras = getIntent().getExtras();
        if(extras != null && extras.containsKey("detail")) {
            int i = extras.getInt("detail");
            Number n = new Number(i);

            TextView tv = findViewById(R.id.detailTextView);
            tv.setText(n.getInfo());
        }
    }
}
