package it.uniroma2.pjdm.esempiolistview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    private final int numberOfElements = 25;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // generate the array
        Number[] numbers = new Number[numberOfElements];
        for(int i = 0; i < numberOfElements; i++) {
           numbers[i] = new Number(i);
        }

        // create the ArrayAdapter
        ArrayAdapter<Number> arrayAdapter =
                new ArrayAdapter<Number>(this,
                        android.R.layout.simple_list_item_1,
                        numbers);

        // associate the ArrayAdapter to the ListView
        ListView listView = findViewById(R.id.listview1);
        listView.setAdapter(arrayAdapter);

        // set a listener for the click (on one of the elements) event
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent detailItent = new Intent();
                detailItent.setClass(MainActivity.this, DetailActivity.class);
                detailItent.putExtra("detail", i);
                startActivity(detailItent);
            }
        });
    }
}







